import * as actionTypes from 'Constants/ActionTypes';
import fetch from 'isomorphic-fetch';

export const showModal = info => ({
  type: actionTypes.SHOW_MODAL,
  info,
});

export const hideModal = () => ({
  type: actionTypes.HIDE_MODAL,
});

const requestTableData = () => ({
  type: actionTypes.REQUEST_TABLE_DATA,
});

const receiveTableData = (json, isIncreasing) => ({
  type: actionTypes.RECEIVE_TABLE_DATA,
  data: json,
  isIncreasing,
});

const fetchTableData = () => (dispatch, getState) => {
  const { limit, page } = getState().table;
  dispatch(requestTableData());
  return fetch(
    `/api/data?_page=${page + 1}&_limit=${limit}`, {
      mode: 'same-origin',
    })
    .then((response) => {
      if (response.status !== 200) {
        return false;
      }
      return response.json();
    })
    .then((json) => {
      dispatch(receiveTableData(json));
    })
    .catch((error) => {
      dispatch(showModal(`Error occured: ${error}`));
    },
  );
};

export const shouldFetchTableIfNeeded = () => (dispatch, getState) => {
  const { isFetching } = getState().table;
  if (isFetching) return false;
  return dispatch(fetchTableData());
};

export const shouldGetDataIfNeeded = () => (dispatch, getState) => {
  const { isDataEndReached } = getState().table;
  if (isDataEndReached) return false;
  return dispatch(shouldFetchTableIfNeeded());
};
