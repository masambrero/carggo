import {
  REQUEST_TABLE_DATA,
  RECEIVE_TABLE_DATA,
} from 'Constants/ActionTypes';

const initialState = {
  sortedColumn: 0,
  sortAscending: true,
  sortActive: false,
  limit: 20,
  pageLimit: 4,
  page: 0,
  isFetching: false,
  data: [],
  isDataEndReached: false,
  headers: [
    {
      type: 'number',
      name: 'field1',
    },
    {
      type: 'number',
      name: 'field2',
    },
    {
      type: 'boolean',
      name: 'field3',
    },
    {
      type: 'link',
      name: 'field4',
    },
    {
      type: 'number',
      name: 'field5',
    },
    {
      type: 'string',
      name: 'field6',
    },
  ],
};

const table = (state = initialState, action) => {
  let data;
  let page;
  let isDataEndReached = false;
  switch (action.type) {
    case REQUEST_TABLE_DATA:
      return {
        ...state,
        isFetching: true,
      };
    case RECEIVE_TABLE_DATA:
      if (action.data.length === 0) {
        return {
          ...state,
          isFetching: false,
          isDataEndReached: true,
        };
      }
      if (action.data.length < state.limit) isDataEndReached = true;
      page = state.page + 1;
      data = [...state.data, ...action.data];
      return {
        ...state,
        page,
        data,
        isDataEndReached,
        isFetching: false,
      };
    default:
      return state;
  }
};

export default table;
