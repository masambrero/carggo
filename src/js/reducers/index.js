import { combineReducers } from 'redux';
import table from './table';
import modal from './modal';

const rootReducer = combineReducers({
  table,
  modal,
});

export default rootReducer;
