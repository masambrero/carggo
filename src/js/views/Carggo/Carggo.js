import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import {
  shouldGetDataIfNeeded,
} from 'Actions';
import './Carggo.css';

class Carggo extends Component {
  static propTypes = {
    shouldGetDataIfNeeded: PropTypes.func.isRequired,
    tableHeaders: PropTypes.arrayOf(
      PropTypes.shape({
        type: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
      }).isRequired,
    ).isRequired,
    tableData: PropTypes.arrayOf(
      PropTypes.arrayOf(
        PropTypes.oneOfType([
          PropTypes.number,
          PropTypes.string,
          PropTypes.bool,
        ]).isRequired,
      ).isRequired,
    ),
  }


  static defaultProps = {
    tableData: [],
  }

  static renderFirstChild(component) {
    const childrenArray = React.Children.toArray(component.children);
    return childrenArray[0] || null;
  }

  static compareFields(a, b, columnIndex, columnType) {
    const urlTypes = [/localhost/, /http:\/\//, /https:\/\//, /ftp:\/\//];
    let aMatch = -1;
    let bMatch = -1;
    switch (columnType) {
      case 'number':
        return a[columnIndex] - b[columnIndex];
      case 'link':
        urlTypes.forEach((regexp, regexpIndex) => {
          if (regexp.test(a[columnIndex])) aMatch = regexpIndex;
          if (regexp.test(b[columnIndex])) bMatch = regexpIndex;
        });
        return aMatch - bMatch;
      case 'string':
      default:
        return a[columnIndex].length - b[columnIndex].length;
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      tableData: [],
      sortedColumn: 0,
      sortAscending: true,
      firstTableRow: null,
      lastTableRow: null,
    };
  }

  componentDidMount() {
    this.props.shouldGetDataIfNeeded();
    window.addEventListener('scroll', this.updateTableData);
  }

  componentWillReceiveProps(nextProps) {
    const { tableData } = nextProps;
    this.setState({
      tableData,
    });
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.updateTableData);
  }

  onSortButtonClick = (e) => {
    if (e.target.dataset.type === 'boolean') return false;
    return this.setState({
      sortedColumn: +e.target.dataset.index,
      sortAscending: !this.state.sortAscending,
    });
  }

  updateTableData = () => {
    const { scrollTop, scrollHeight, clientHeight } = document.querySelectorAll('.carggo-table tbody')[0];
    if (scrollHeight - clientHeight <= scrollTop + 100) this.props.shouldGetDataIfNeeded();
  }


  sortData = (data) => {
    const columnType = this.props.tableHeaders[this.state.sortedColumn].type;
    const columnIndex = this.state.sortedColumn;
    const tableData = data.sort((a, b) => (
      this.state.sortAscending === true ?
        this.constructor.compareFields(a, b, columnIndex, columnType) :
        -(this.constructor.compareFields(a, b, columnIndex, columnType))
    ));
    return tableData;
  }

  formatField(data, dataIndex) {
    let formattedData;
    this.props.tableHeaders.forEach((header, headerIndex) => {
      if (headerIndex === dataIndex) {
        switch (header.type) {
          case 'boolean':
            formattedData = data.toString();
            break;
          case 'number':
            formattedData = (data >= 0) ?
              <span className="number number--positive">{Math.floor(data)}</span> :
              <span className="number number--negative">{Math.floor(data)}</span>;
            break;
          case 'link':
            formattedData = (/^(http|https|ftp):\/\//.test(data)) ?
              <a href={data} target="_blank" rel="noopener noreferrer">{data}</a> :
              <a href={`http://${data}`} target="_blank" rel="noopener noreferrer">{data}</a>;
            break;
          default:
            formattedData = data;
        }
      }
    });
    return formattedData;
  }

  render() {
    const { sortedColumn: columnIndex, sortAscending } = this.state;
    return (
      <div className="carggo">
        <div className="carggo-table" >
          <table>
            <caption>Таблица с сортировкой и дозагрузкой данных</caption>
            <thead>
              <tr>
                {this.props.tableHeaders.map((header, headerIndex) => (
                  <th
                    key={header.name}
                    className={`carggo-table__header ${headerIndex === columnIndex ? 'carggo-table__header--active' : ''}`}
                  >
                    <button
                      className="carggo-table__sort-button"
                      data-index={headerIndex}
                      data-type={header.type}
                      onClick={this.onSortButtonClick}
                    >
                      {header.name}
                    </button>
                    <CSSTransitionGroup
                      component={this.constructor.renderFirstChild}
                      transitionName="arrow"
                      transitionEnterTimeout={800}
                      transitionLeaveTimeout={800}
                    >
                      {headerIndex === columnIndex &&
                        <img
                          className={`carggo-table__arrow ${sortAscending ?
                            'carggo-table__arrow--up' :
                            'carggo-table__arrow--down'}`}
                          src="/images/arrow.svg" alt="sort arrow"
                        />
                      }
                    </CSSTransitionGroup>
                  </th>
                ))}
              </tr>
            </thead>
            <CSSTransitionGroup
              component={this.constructor.renderFirstChild}
              transitionName="table"
              transitionEnterTimeout={2000}
              transitionLeave={false}
            >
              {!!this.state.tableData.length &&
                <tbody onScroll={() => (this.updateTableData())}>
                  {this.sortData(this.state.tableData).map((row, rowIndex) => (
                    <tr key={rowIndex}>
                      {row.map((data, dataIndex) => (
                        <td key={dataIndex}>
                          {this.formatField(data, dataIndex)}
                        </td>
                      ))}
                    </tr>
                  ))}
                </tbody>
              }
            </CSSTransitionGroup>
          </table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  tableHeaders: state.table.headers,
  tableData: state.table.data,
  isFetching: state.table.isFetching,
  sortedColumn: state.table.sortedColumn,
  sortAscending: state.table.sortAscending,
  sortActive: state.table.sortActive,
});

const mapDispatchToProps = dispatch => ({
  shouldGetDataIfNeeded: bindActionCreators(shouldGetDataIfNeeded, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Carggo);
