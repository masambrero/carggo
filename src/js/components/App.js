import React from 'react';
import { IndexRoute, Router, Route, browserHistory } from 'react-router';
import MainFrame from 'Containers/MainFrame';
import NotFound from 'Views/NotFound/NotFound';

const App = () => (
  <Router history={browserHistory}>
    <Route path="/" component={MainFrame}>
      <IndexRoute
        onEnter={() => window.scrollTo(0, 0)}
        getComponent={(location, callback) => {
          require.ensure([], (require) => {
            callback(null, require('../views/Home/Home').default);
          });
        }}
      />
      <Route
        path="/carggo"
        onEnter={() => window.scrollTo(0, 0)}
        getComponent={(location, callback) => {
          require.ensure([], (require) => {
            callback(null, require('../views/Carggo/Carggo').default);
          });
        }}
      />
      <Route path="*" onEnter={this.scrollTop} component={NotFound} />
    </Route>
  </Router>
);

export default App;
